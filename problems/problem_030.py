# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(*numbers):


    if len(numbers) == 0:
        return None

    new_list = sorted(numbers)
    return new_list[-2]

x = find_second_largest(1, 4, 6, 9)
print(x)
